package main.java;


/**
 * Klasa odpowiedzialna za utworzenie reprezentacji punktów zawodników
 *
 */
public class Score {

    private Player player;
    private String date;
    private Double  distance;
    private Double  time;

    public Score(Player player, String date, Double distance, Double time) {
        this.player = player;
        this.date = date;
        this.distance = distance;
        this.time = time;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }
}
