package main.java;

import java.sql.*;

/**
 * Klasa odpowiedzialna za utworzenie bazy danych, stworzenie oraz uzupełnienie tabeli
 * oraz zapytania
 */

public class DatabaseConnector {

    private static String url = "jdbc:sqlite:C:/Users/m_sur/IdeaProjects/player/src/main/resources/exercise.db";

    /**
     * Metoda odpowiedzialna za stworzenie nowej bazy danych
     */
    public static void createNewDatabase() {

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Błąd przy tworzeniu bazy danych");
        }
    }

    /**
     * Metoda odpowiedzialna za stworzenie nowej bazy danych (jesli nie istnieje)
     * oraz wyczyszczenie je zawartości jeśli została wcześniej uzupełniona
     */
    public static void createNewTable() {

        String sql = "CREATE TABLE IF NOT EXISTS scores (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text NOT NULL,\n"
                + "	country text NOT NULL,\n"
                + "	date text NOT NULL,\n"
                + "	distance real NOT NULL,\n"
                + "	time real NOT NULL\n"
                + ");";
        String sqlDelete = " DELETE FROM scores;";

        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {

            stmt.execute(sql);
            stmt.execute(sqlDelete);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("błąd przy tworzeniu tabeli");
        }
    }

    /**
     * Metoda odpowiedzialna za połączenie do bazy danych
     */
    private Connection connect() {

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Błąd przy połączeniu do bazy danych");
        }
        return conn;
    }
    /**
     * Metoda odpowiedzialna za uzupełnienie tabeli "scores" danymi
     */
    public void insert(String name, String country, String date, Double distance, Double time) {
        String sql = "INSERT INTO scores (name, country, date, distance, time) VALUES(?,?,?,?,?)";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, country);
            pstmt.setString(3, date);
            pstmt.setDouble(4, distance);
            pstmt.setDouble(5, time);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Błąd przy wstawianiu  danych");
        }
    }

    /**
     * Metoda odpowiedzialna za wskazanie śrenich czasów zawodnkiów na 1000m
     */
    public void selectAverageTime(){
        String sql = "SELECT  name AS 'Player', ROUND(SUM(time)/SUM(distance)*1000, 2) AS 'Average Time' FROM scores GROUP BY name Order BY 'Average Time';";

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
                System.out.println(rs.getString("Player") + " - "  + "1000m w "+
                        rs.getDouble("Average Time")+"s");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Błąd w zapytaniu SELECT - selectAverageTime()");
        }
    }

    /**
     * Metoda odpowiedzialna za znalezienie najwcześniejszej daty zanotowanej przez zawodników
     */
    public void selectEarliestDate(){
        String sql="select date from scores order by date limit 1;";
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
                System.out.println("\n\nPierwszy wynik zarejestrowano dnia: "+ rs.getString("Date")) ;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Błąd w zapytaniu SELECT - selectEarliestDate()");
        }

    }

    /**
     * Metoda odpowiedzialna za znalezienie trzech krajów z których zawonicy najczęściej biorą udział w zawodach
     */
    public void selectMostCommonCountries(){
        String sql="select country, count(country) as common From scores group by country order by common desc limit 3;";
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
                System.out.println( rs.getString("Country"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Błąd w zapytaniu SELECT - selectMostCommonCountries()");
        }
    }
}
