package main.java;

/**
 * Klasa odpowiedzialna za utworzenie schematu zawonika
 * składającego się z nazwiska oraz kraju z którego pochodzi
 */
public class Player {

    private String name;
    private String country;


    public Player(String name, String country) {
        this.name = name;
        this.country = country;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
