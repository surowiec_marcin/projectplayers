package main.java;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;


/**
 * Klasa Main, wywołuje połączenie z bazą danych, uruchamia odczyt danych z plików oraz wyświetla
 * w konsoli dane na temat zawodników
 */
public class Main {

    private static DatabaseConnector databaseConnector=new DatabaseConnector();
    public static void main(String[] args) {

        DatabaseConnector.createNewDatabase();
        DatabaseConnector.createNewTable();

        ReadFile readFile = new ReadFile();


        if(args.length==0 ){
            System.out.println("Proszę jako argument podać nazwę pliku lub ścieżkę dostępu do plików");
            System.exit(0);
        }
        else if(args[0].contains(".txt")) {
            String dir = System.getProperty("user.dir");

            try {
                readFile.readFile(dir+"\\"+args[0]);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.print("Nie znaleziono pliku");
            }

        }
        else if(args[0].contains("\\")){

            File f = new File(args[0]);

            FilenameFilter textFilter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".txt");
                }
            };

            File[] files = f.listFiles(textFilter);
            try {
                for (File file : files) {
                    if (file.isDirectory()) {
                        try {
                            readFile.readFile(file.getCanonicalPath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            readFile.setLineWithNewPlayer(0);
                            readFile.setCounter(0);
                            readFile.readFile(file.getCanonicalPath());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }catch (NullPointerException e){
                System.out.println("Podana ścieżka dostępu nie zawiera plików");
            }

        }

        else {
            System.out.println("Nieprawidłowy parametr");
        }

        databaseConnector.selectAverageTime();
        databaseConnector.selectEarliestDate();
        System.out.println("\nTop 3 państwa:");
        databaseConnector.selectMostCommonCountries();

    }


}
