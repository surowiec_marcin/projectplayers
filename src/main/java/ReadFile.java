package main.java;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa odpowiedzialna obsługująca odczyt plików oraz zapis odczytanych danych do bazy danych
 */
public class ReadFile {

    private int lineWithNewPlayer=0;
    private int counter =0;
    private List<Score> scoreList=new ArrayList<>();
    private DatabaseConnector databaseConnector=new DatabaseConnector();

    /**
     * Metoda odpowiedzialna za odczyt plików oraz zapis odczytanych danych do bazy danych
     */
    public  void readFile( String file) throws IOException {

        Score score;
        Player player;

        String name="";
        String coutry="";
        String  date;
        Double  distance;
        Double  time;

        BufferedReader br = null;
        try {

            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line;

        try {
            while ((line = br.readLine()) != null) {
                counter++;
                if (counter==(1+lineWithNewPlayer)){
                    name=line;

                }
                else if(counter==(2+lineWithNewPlayer)){
                    coutry=line;

                }
                else if((counter>=(3+lineWithNewPlayer) && line.contains("|"))){
                        String[] tokens = line.split("\\|");
                        date=tokens[0];
                        distance=(Double.valueOf(tokens[1]));
                        time=(Double.valueOf(tokens[2]));
                        player=new Player(name, coutry);
                        score=new Score(player, date, distance, time);
                        scoreList.add(score);
                        databaseConnector.insert(name, coutry, date, distance, time);

                 }
                    else {
                    lineWithNewPlayer=counter;
                    }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();

    }

    public List<Score> getScoreList() {
        return scoreList;
    }

    public void setLineWithNewPlayer(int lineWithNewPlayer) {
        this.lineWithNewPlayer = lineWithNewPlayer;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
